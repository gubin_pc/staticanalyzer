package analyzer;

import analyzer.ast.parser.Java8Lexer;
import analyzer.ast.parser.Java8Parser;
import analyzer.ast.visitorsImpl.MainVisitor;
import analyzer.statements.Statement;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Analyzer {
    List<String> pathToFiles;

    public Analyzer() {
        pathToFiles = new ArrayList<>();
    }

    public void add(String path) {
        pathToFiles.add(path);
    }

    public void analyze() throws IOException {
        if (pathToFiles != null) {
            System.out.println(pathToFiles);

            InputStream is = System.in;
            if (pathToFiles == null) {
                return;
            }

            Statement statement = null;
            for (String path : pathToFiles) {
                is = new FileInputStream(path);
                ANTLRInputStream input = new ANTLRInputStream(is);
                Java8Lexer lexer = new Java8Lexer(input);
                CommonTokenStream token = new CommonTokenStream(lexer);
                Java8Parser parser = new Java8Parser(token);
                ParseTree tree = parser.compilationUnit();
                MainVisitor visitor = new MainVisitor();
                visitor.visit(tree);
                if (statement == null){
                    statement = visitor.getStatement();
                } else {
                    statement.add(visitor.getStatement().get(0));
                }
            }

//            List<AbstractVariable> variableStatementList = new ArrayList<>();
//            List<Statement> abstractStatementList = new ArrayList<>();
//            List<Statement> methodDeclarationStatementList = new ArrayList<>();
//
//            Finder.findAllClass(statement, abstractStatementList);
//            Finder.findAllMethods(statement, methodDeclarationStatementList);
//            Finder.findAllVariables(statement, variableStatementList);
        }
    }

}
