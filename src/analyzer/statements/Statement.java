package analyzer.statements;

import analyzer.statements.variables.AbstractVariable;

import java.util.LinkedList;
import java.util.List;

public interface Statement {
    String getName();

    void add(Statement statement);

    void addAll(List<Statement> list);

    List<Statement> getStatements();

    Statement getParent();

    List<Statement> findStatement(String name);

    Statement get(int i);

    void overwriteStatements(List<Statement> statements);

    LinkedList<AbstractVariable> getVariables();

    int line();
}
