package analyzer.statements;

public interface MethodStatement extends Statement{
    int getNumberArguments();
}
