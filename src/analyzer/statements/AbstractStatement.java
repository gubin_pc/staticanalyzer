package analyzer.statements;

import analyzer.statements.impl.legacy.statement.Finder;
import analyzer.statements.variables.AbstractVariable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractStatement implements Statement{
    protected Statement parent;
    protected List<Statement> statements;
    protected LinkedList<AbstractVariable> variables;
    protected String name;
    protected int line;

    public AbstractStatement() {
        statements = new ArrayList<>();
        variables = new LinkedList<>();
    }

    public AbstractStatement(Statement parent) {
        statements = new ArrayList<>();
        variables = new LinkedList<>();
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void add(Statement statement) {
        statements.add(statement);
    }

    public Statement get(int i) {
        return statements.get(i);
    }

    public void addAll(List<Statement> list) {
        if (list != null)
            statements.addAll(list);
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public void overwriteStatements(List<Statement> statements) {
        this.statements = statements;
    }

    public List<Statement> findStatement(String name) {
        Statement firstParent = parent;
        while (firstParent.getParent() != null){
            firstParent = firstParent.getParent();
        }
        List<Statement> findStatements = new ArrayList<>();
        Finder.findStatementRecurrent(name, parent, findStatements);
        return findStatements;
    }

    public Statement getParent() {
        return parent;
    }

    public LinkedList<AbstractVariable> getVariables() {
        return variables;
    }

    public void setVariables(LinkedList<AbstractVariable> variables) {
        this.variables = variables;
    }

    public AbstractVariable findVariable(Statement statement, String name) {
        for (AbstractVariable variable : statement.getVariables()) {
            if (variable.getName().equals(name)) {
                return variable;
            }
        }
        return findVariable(this.parent, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractStatement)) return false;

        AbstractStatement that = (AbstractStatement) o;

        if (parent != null ? !parent.equals(that.parent) : that.parent != null) return false;
        if (statements != null ? !statements.equals(that.statements) : that.statements != null) return false;
        if (variables != null ? !variables.equals(that.variables) : that.variables != null) return false;
        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode() {
        int result = parent != null ? parent.getName().hashCode() : 0;
        result = 31 * result + (statements != null ? statements.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public int line() {
        return line;
    }


    public void setLine(int line) {
        this.line = line;
    }
}
