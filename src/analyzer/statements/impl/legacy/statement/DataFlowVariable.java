package analyzer.statements.impl.legacy.statement;

import org.antlr.v4.runtime.Token;

import java.util.ArrayList;
import java.util.List;

public class DataFlowVariable {
    private Token token;
    private List<DataFlowVariable> usingOn;

    public DataFlowVariable(Token token) {
        this.token = token;
        usingOn = new ArrayList<>();
    }

    public List<DataFlowVariable> getUsingOn() {
        return usingOn;
    }
}
