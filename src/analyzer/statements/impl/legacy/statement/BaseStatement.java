package analyzer.statements.impl.legacy.statement;

import analyzer.statements.AbstractStatement;

public class BaseStatement extends AbstractStatement {
    public BaseStatement() {
        name = "Base Statement";
    }

    public BaseStatement(String name){
        this.name = name;
    }
}
