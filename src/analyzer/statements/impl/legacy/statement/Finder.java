package analyzer.statements.impl.legacy.statement;

import analyzer.statements.Statement;
import analyzer.statements.impl.statements.ClassDeclarationStatement;
import analyzer.statements.impl.statements.InterfaceDeclarationStatement;
import analyzer.statements.impl.statements.MethodDeclarationStatement;
import analyzer.statements.variables.AbstractVariable;
import org.antlr.v4.runtime.Token;

import java.util.List;

public class Finder {
    static public void findStatementRecurrent(String name, Statement from, List<Statement> saveList){
        if (from.getStatements().size() != 0) {
            for (Statement as : from.getStatements()) {
                findStatementRecurrent(name, as, saveList);
            }
        }

        if (from.getName().equals(name)) {
            saveList.add(from);
        }
    }


    static public void findVariableRecurrent(String name, Statement from, List<AbstractVariable> saveList){
        if (from.getStatements().size() != 0) {
            for (Statement as : from.getStatements()) {
                findVariableRecurrent(name, as, saveList);
            }
        }

        for (AbstractVariable var : from.getVariables()){
            if(var.getName().equals(name)){
                saveList.add(var);
            }
        }
    }

    static public void findAllMethods(Statement from, List<Statement> saveList){
        if (from.getStatements().size() != 0) {
            for (Statement as : from.getStatements()) {
                findAllMethods(as, saveList);
            }
        }

        if (from instanceof MethodDeclarationStatement){
            saveList.add(from);
        }
    }

    static public void findAllClass(Statement from, List<Statement> saveList){
        if (from.getStatements().size() != 0) {
            for (Statement as : from.getStatements()) {
                findAllClass(as, saveList);
            }
        }

        if (from instanceof ClassDeclarationStatement){
            saveList.add(from);
        }
    }

    static public void findAllInterface(Statement from, List<Statement> saveList){
        if (from.getStatements().size() != 0) {
            for (Statement as : from.getStatements()) {
                findAllInterface(as, saveList);
            }
        }

        if (from instanceof InterfaceDeclarationStatement){
            saveList.add(from);
        }
    }

    static public void findAllVariables(Statement from, List<AbstractVariable> saveList){
        if (from.getStatements().size() != 0) {
            for (Statement as : from.getStatements()) {
                findAllVariables(as, saveList);
            }
        }

        for (AbstractVariable var : from.getVariables()){
            saveList.add(var);
        }
    }

    static public AbstractVariable findVariableByToken(Token token, List<AbstractVariable> list){
        for (AbstractVariable variable : list) {
            if (variable.getDeclaratorIdContext().getStart().equals(token)){
                return variable;
            }
        }
        return null;
    }

    static public AbstractVariable findVariableInContext(Statement ctx, List<AbstractVariable> list, String name){
        for (AbstractVariable variable : list) {
            if (name.equals(variable.getName()) && variable.getBelongs().equals(ctx)){
                return variable;
            }
        }
        return null;
    }

    static public AbstractVariable findVariableRecurrentUp(Statement ctx, List<AbstractVariable> list, String name){
        AbstractVariable tmp = findVariableInContext(ctx, list, name);
        if (tmp == null && ctx.getParent() != null){
            tmp = findVariableRecurrentUp(ctx.getParent(), list, name);
        }
        return tmp;
    }

    static public AbstractVariable findVariableRecurrentDown(Statement ctx, List<AbstractVariable> list, String name){
        AbstractVariable tmp = findVariableInContext(ctx, list, name);
        if (tmp == null && ctx.getParent() != null){
            for (Statement statement : ctx.getStatements()) {
                tmp = findVariableRecurrentDown(statement, list, name);
                if (tmp != null){
                    return tmp;
                }
            }
        }
        return tmp;
    }

    static public Statement findStatement(String name, List<Statement> list) {
        for (Statement statement : list) {
            if (statement.getName().equals(name)){
                return statement;
            }
        }
        return null;
    }
}
