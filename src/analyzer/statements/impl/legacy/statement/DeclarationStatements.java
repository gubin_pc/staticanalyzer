package analyzer.statements.impl.legacy.statement;

import analyzer.statements.impl.statements.ClassDeclarationStatement;
import analyzer.statements.impl.statements.MethodDeclarationStatement;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.HashSet;
import java.util.Set;

public class DeclarationStatements {
    private ParseTree tree;
    private Set<ClassDeclarationStatement> classStatements;
    private Set<MethodDeclarationStatement> methodStatements;

    public DeclarationStatements(ParseTree tree) {
        this.tree = tree;
        classStatements = new HashSet<ClassDeclarationStatement>();
        methodStatements = new HashSet<MethodDeclarationStatement>();
    }

    public void execute() {


    }
}
