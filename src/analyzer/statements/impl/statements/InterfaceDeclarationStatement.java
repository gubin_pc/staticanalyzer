package analyzer.statements.impl.statements;

import analyzer.ast.parser.Java8Parser;
import analyzer.statements.AbstractStatement;
import analyzer.statements.Statement;

public class InterfaceDeclarationStatement extends AbstractStatement {
    Java8Parser.NormalInterfaceDeclarationContext iDC;

    public InterfaceDeclarationStatement(Java8Parser.NormalInterfaceDeclarationContext iDC) {
        super();
        this.iDC = iDC;
        this.name = iDC.Identifier().getText();
    }

    public InterfaceDeclarationStatement(Java8Parser.NormalInterfaceDeclarationContext iDC, Statement parent) {
        super(parent);
        this.iDC = iDC;
        this.name = iDC.Identifier().getText();
    }
}
