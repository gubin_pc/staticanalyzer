package analyzer.statements.impl.statements;

import analyzer.ast.parser.Java8Parser;
import analyzer.statements.AbstractStatement;
import analyzer.statements.MethodStatement;
import analyzer.statements.Statement;

public class MethodInvoked extends AbstractStatement implements MethodStatement{
    private Object ctx;

    public MethodInvoked(Object ctx, Statement parent) {
        super(parent);
        this.ctx = ctx;
        setLine();
    }

    private int setLine() {
        if (ctx instanceof Java8Parser.MethodInvocationContext) {
            try {
                return (((Java8Parser.MethodInvocationContext) ctx).argumentList().expression().size());
            } catch (NullPointerException e) {
                return 0;
            }
        }
        if (ctx instanceof Java8Parser.MethodInvocation_lf_primaryContext) {
            try {
                return ((Java8Parser.MethodInvocation_lf_primaryContext) ctx).argumentList().expression().size();
            } catch (NullPointerException e) {
                return 0;
            }
        }
        if (ctx instanceof Java8Parser.MethodInvocation_lfno_primaryContext) {
            try {
                return ((Java8Parser.MethodInvocation_lfno_primaryContext) ctx).argumentList().expression().size();
            } catch (NullPointerException e) {
                return 0;
            }
        }
        return 0;
    }

    @Override
    public String getName() {
        if (ctx instanceof Java8Parser.MethodInvocationContext) {
            Java8Parser.MethodInvocationContext context =  (Java8Parser.MethodInvocationContext) ctx;
            if (context.methodName() != null){
                return context.methodName().getText();
            }
            if (context.Identifier() != null) {
                return context.Identifier().getText();
            }
        }
        if (ctx instanceof Java8Parser.MethodInvocation_lf_primaryContext) {
            return ((Java8Parser.MethodInvocation_lf_primaryContext) ctx).Identifier().getText();
        }
        if (ctx instanceof Java8Parser.MethodInvocation_lfno_primaryContext) {
            Java8Parser.MethodInvocation_lfno_primaryContext context =  (Java8Parser.MethodInvocation_lfno_primaryContext) ctx;
            if (context.methodName() != null){
                return context.methodName().getText();
            }
            if (context.Identifier() != null) {
                return context.Identifier().getText();
            }
        }
        return null;
    }

    @Override
    public int getNumberArguments(){
        int size = 0;
        Java8Parser.ArgumentListContext context = null;
        if (ctx instanceof Java8Parser.MethodInvocationContext) {
            context = ((Java8Parser.MethodInvocationContext) ctx).argumentList();
        }
        if (ctx instanceof Java8Parser.MethodInvocation_lf_primaryContext) {
            context = ((Java8Parser.MethodInvocation_lf_primaryContext) ctx).argumentList();
        }
        if (ctx instanceof Java8Parser.MethodInvocation_lfno_primaryContext) {
            context = ((Java8Parser.MethodInvocation_lfno_primaryContext) ctx).argumentList();
        }
        if (context != null){
            size = context.expression().size();
        }
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MethodInvoked)) return false;
        if (!super.equals(o)) return false;

        MethodInvoked that = (MethodInvoked) o;

        return !(ctx != null ? !ctx.equals(that.ctx) : that.ctx != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (ctx != null ? ctx.hashCode() : 0);
        return result;
    }
}
