package analyzer.statements.impl.statements;

import analyzer.ast.parser.Java8Parser;
import analyzer.statements.AbstractStatement;
import analyzer.statements.MethodStatement;
import analyzer.statements.Statement;

public class MethodDeclarationStatement extends AbstractStatement implements MethodStatement {
    Java8Parser.MethodDeclarationContext mDC;
    Java8Parser.InterfaceMethodDeclarationContext iMDC;

    public MethodDeclarationStatement(Java8Parser.MethodDeclarationContext mDC) {
        super();
        this.mDC = mDC;
        this.name = mDC.methodHeader().methodDeclarator().Identifier().getText();
    }

    public MethodDeclarationStatement(Java8Parser.MethodDeclarationContext mDC, Statement parent) {
        super(parent);
        this.mDC = mDC;
        this.name = mDC.methodHeader().methodDeclarator().Identifier().getText();
    }


    public MethodDeclarationStatement(Java8Parser.InterfaceMethodDeclarationContext mDC) {
        super();
        this.iMDC = mDC;
        this.name = mDC.methodHeader().methodDeclarator().Identifier().getText();
    }

    public MethodDeclarationStatement(Java8Parser.InterfaceMethodDeclarationContext mDC, Statement parent) {
        super(parent);
        this.iMDC = mDC;
        this.name = mDC.methodHeader().methodDeclarator().Identifier().getText();
    }

    public Java8Parser.MethodDeclarationContext getmDC() {
        if (mDC != null) {
            return mDC;
        }
        return null;
    }

    @Override
    public int getNumberArguments() {
        int size = 0;
        Java8Parser.FormalParameterListContext context = null;
        if (mDC != null && mDC.methodHeader().methodDeclarator().formalParameterList() != null) {
            context = mDC.methodHeader().methodDeclarator().formalParameterList();
        }
        if (iMDC != null && iMDC.methodHeader().methodDeclarator().formalParameterList() != null) {
            context = iMDC.methodHeader().methodDeclarator().formalParameterList();
        }
        if (context != null) {
            if (context.formalParameters() != null) {
                size += context.formalParameters().formalParameter().size();
            }
            if (context.lastFormalParameter() != null) {
                size++;
            }
        }
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MethodDeclarationStatement)) return false;
        if (!super.equals(o)) return false;

        MethodDeclarationStatement that = (MethodDeclarationStatement) o;

        if (mDC != null ? !mDC.equals(that.mDC) : that.mDC != null) return false;
        return !(iMDC != null ? !iMDC.equals(that.iMDC) : that.iMDC != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mDC != null ? mDC.hashCode() : 0);
        result = 31 * result + (iMDC != null ? iMDC.hashCode() : 0);
        return result;
    }
}
