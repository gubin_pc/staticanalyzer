package analyzer.statements.impl.statements;

import analyzer.ast.parser.Java8Parser;
import analyzer.statements.AbstractStatement;
import analyzer.statements.Statement;

public class ClassDeclarationStatement  extends AbstractStatement {
    Java8Parser.NormalClassDeclarationContext cDC;

    public ClassDeclarationStatement(Java8Parser.NormalClassDeclarationContext cDC) {
        super();
        this.cDC = cDC;
        this.name = cDC.Identifier().getText();
    }

    public ClassDeclarationStatement(Java8Parser.NormalClassDeclarationContext cDC, Statement parent) {
        super(parent);
        this.cDC = cDC;
        this.name = cDC.Identifier().getText();
    }
}
