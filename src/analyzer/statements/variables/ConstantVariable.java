package analyzer.statements.variables;

import analyzer.ast.parser.Java8Parser;

import java.util.List;

public class ConstantVariable extends AbstractVariable {
    List<Java8Parser.ConstantModifierContext> constantModifier;

    public List<Java8Parser.ConstantModifierContext> getConstantModifier() {
        return constantModifier;
    }

    public void setConstantModifier(List<Java8Parser.ConstantModifierContext> constantModifier) {
        this.constantModifier = constantModifier;
    }
}
