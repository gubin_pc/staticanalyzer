package analyzer.statements.variables;

import analyzer.ast.parser.Java8Parser;
import analyzer.statements.Statement;
import analyzer.statements.impl.legacy.statement.DataFlowVariable;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractVariable {
    protected Java8Parser.UnannTypeContext unannTypeContext;
    protected Java8Parser.VariableDeclaratorIdContext variableDeclaratorIdContext;

    private Map<AbstractVariable, Integer> usedIn;
    private Map<AbstractVariable, Integer> dependsOn;

    protected Statement belongs;

    DataFlowVariable dataFlowVariable;

    public AbstractVariable() {
        usedIn = new LinkedHashMap<>();
        dependsOn = new LinkedHashMap<>();
    }

    public Java8Parser.UnannTypeContext getUnannTypeContext() {
        return unannTypeContext;
    }

    public void setUnannTypeContext(Java8Parser.UnannTypeContext unannTypeContext) {
        this.unannTypeContext = unannTypeContext;
    }

    public Java8Parser.VariableDeclaratorIdContext getDeclaratorIdContext() {
        return variableDeclaratorIdContext;
    }

    public void setDeclaratorContext(Java8Parser.VariableDeclaratorIdContext declaratorContext) {
        this.variableDeclaratorIdContext = declaratorContext;
    }

    public Map<AbstractVariable, Integer> getUsedIn() {
        return usedIn;
    }

    public void setUsedIn(Map<AbstractVariable, Integer> usedIn) {
        this.usedIn = usedIn;
    }

    public Map<AbstractVariable, Integer> getDependsOn() {
        return dependsOn;
    }

    public void setDependsOn(Map<AbstractVariable, Integer> dependsOn) {
        this.dependsOn = dependsOn;
    }

    public Statement getBelongs() {
        return belongs;
    }

    public void setBelongs(Statement belongs) {
        this.belongs = belongs;
    }

    public String getName() {
        return variableDeclaratorIdContext.Identifier().getText();
    }

    public DataFlowVariable getDataFlowVariable() {
        return dataFlowVariable;
    }

    public void setDataFlowVariable(DataFlowVariable dataFlowVariable) {
        this.dataFlowVariable = dataFlowVariable;
    }
}
