package analyzer.statements.variables;

import analyzer.ast.parser.Java8Parser;

import java.util.List;

public class ArgumentParameterVariable extends AbstractVariable{
    List<Java8Parser.VariableModifierContext> variableModifier;

    public List<Java8Parser.VariableModifierContext> getVariableModifier() {
        return variableModifier;
    }

    public void setVariableModifier(List<Java8Parser.VariableModifierContext> variableModifier) {
        this.variableModifier = variableModifier;
    }
}
