package analyzer.statements.variables;

import analyzer.ast.parser.Java8Parser;

import java.util.List;

public class FieldClassVariable extends AbstractVariable {
    List<Java8Parser.FieldModifierContext>  fieldModifier;

    public List<Java8Parser.FieldModifierContext> getFieldModifier() {
        return fieldModifier;
    }

    public void setFieldModifier(List<Java8Parser.FieldModifierContext> fieldModifier) {
        this.fieldModifier = fieldModifier;
    }
}
