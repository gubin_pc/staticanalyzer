package analyzer.dataflow;

import analyzer.statements.MethodStatement;
import analyzer.statements.Statement;

import java.util.Set;

public interface DFGNode {

    MethodStatement getMethodStatement();

    Set<DFGNode> getInvoked();

    void setInvoked(Statement invoked);

    Set<DFGNode> getCalls();

    void overwriteInvoked(Set<DFGNode> invoked);

    void addToCalls(DFGNode call);

    @Deprecated
    void addToCalls(MethodStatement call);
}
