package analyzer.dataflow.impl.dataflow;

import analyzer.dataflow.DFGNode;
import analyzer.statements.MethodStatement;
import analyzer.statements.Statement;

import java.util.HashSet;
import java.util.Set;

public class DFG_TNode implements DFGNode {

    private MethodStatement method;
    private Set<DFGNode> invoked;

    public DFG_TNode(MethodStatement nameMethod) {
        this.method = nameMethod;
        invoked = new HashSet<>();
    }

    public String getMethodName() {
        return method.getName();
    }

    @Override
    public MethodStatement getMethodStatement() {
        return method;
    }

    @Override
    public Set<DFGNode> getInvoked() {
        return invoked;
    }

    @Override
    public void setInvoked(Statement invoked) {
        this.invoked.add(DataFlow.getInstance().get(invoked));
    }

    @Override
    public Set<DFGNode> getCalls() {
        return null;
    }

    @Override
    public void overwriteInvoked(Set<DFGNode> invoked) {
        this.invoked = invoked;
    }

    @Override
    public void addToCalls(DFGNode call) {
        return;
    }

    @Override
    public void addToCalls(MethodStatement call) {
        return;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DFG_TNode)) return false;

        DFG_TNode dfg_tNode = (DFG_TNode) o;

        return !(method != null ? !method.equals(dfg_tNode.method) : dfg_tNode.method != null);
    }

    @Override
    public int hashCode() {
        return method != null ? method.hashCode() : 0;
    }

    @Override
    public String toString() {
        String str = this.getMethodName() + "#t_node\n";
        str = str + " invoked -> {";
        for (DFGNode node : invoked) {
            str = str + node.getMethodStatement().getName() + ", ";
        }
        str = str + "}\n";
        return str;
    }


}
