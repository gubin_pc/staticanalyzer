package analyzer.dataflow.impl.dataflow;

import analyzer.dataflow.DFGNode;
import analyzer.dataflow.DataFlowGraph;
import analyzer.statements.Statement;

import java.util.HashMap;
import java.util.Map;

public class DataFlow implements DataFlowGraph {
    private static DataFlow ourInstance = null;
    private Map<Integer, DFGNode> dfgNodes;

    private DataFlow() {
        dfgNodes = new HashMap<>();
    }

    public static DataFlowGraph getInstance() {
        if (ourInstance == null)
            ourInstance = new DataFlow();
        return ourInstance;
    }

    @Override
    public DFGNode put(DFGNode node) {
        if (node instanceof DFG_TNode) {
            DFGNode findNode = findMatch(node);
            if (findNode != null) {
                return findNode;
            }
        }
        if (node instanceof DFG_Node) {
            DFGNode findNode = findMatch(node);
            if (findNode != null) {
                node.overwriteInvoked(findNode.getInvoked());
                dfgNodes.remove(findNode.hashCode(), findNode);
            }
        }
        return dfgNodes.put(node.hashCode(), node);

    }

    @Override
    public DFGNode get(Statement statement) {
        return dfgNodes.get(statement.hashCode());
    }

    @Override
    public DFGNode get(int hashKey) {
        return dfgNodes.get(hashKey);
    }

    @Override
    public String toString() {
        String str = "\n\nDataFlowGraph: \n";
        for (DFGNode node : dfgNodes.values()) {
            str += node.toString();
        }
        return str;
    }

    private DFGNode findMatch(DFGNode dfgNode) {
        if (dfgNode instanceof DFG_TNode) {
            return findDFG_Node(dfgNode);
        }
        if (dfgNode instanceof DFG_Node) {
            return findDFG_TNode(dfgNode);
        }

        return null;
    }

    private DFGNode findDFG_TNode(DFGNode dfgNode) {
        for (DFGNode node : dfgNodes.values()) {
            if (node instanceof DFG_Node) {
                continue;
            }
            if (dfgNode.getMethodStatement().getName().equals(node.getMethodStatement().getName()) &&
                    dfgNode.getMethodStatement().getNumberArguments() == node.getMethodStatement().getNumberArguments()) {
                node.getInvoked().addAll(dfgNode.getInvoked());
                return node;
            }
        }
        return null;
    }

    private DFGNode findDFG_Node(DFGNode dfgNode) {
        for (DFGNode node : dfgNodes.values()) {
            if (node instanceof DFG_TNode) {
                continue;
            }
            if (dfgNode.getMethodStatement().getName().equals(node.getMethodStatement().getName()) &&
                    dfgNode.getMethodStatement().getNumberArguments() == node.getMethodStatement().getNumberArguments()) {
                node.getInvoked().addAll(dfgNode.getInvoked());
                return node;
            }

        }
        return null;
    }


}
