package analyzer.dataflow.impl.dataflow;

import analyzer.dataflow.DFGNode;
import analyzer.statements.MethodStatement;
import analyzer.statements.Statement;

import java.util.HashSet;
import java.util.Set;

public class DFG_Node implements DFGNode {
    private MethodStatement method;
    private Set<DFGNode> calls;
    private Set<DFGNode> invoked;

    public DFG_Node(MethodStatement method) {
        calls = new HashSet<>();
        invoked = new HashSet<>();
        this.method = method;
    }

    @Override
    public MethodStatement getMethodStatement() {
        return method;
    }

    public String getMethodName() {
        return method.getName();
    }

    @Override
    public Set<DFGNode> getInvoked() {
        return invoked;
    }

    @Override
    public void setInvoked(Statement invoked) {
        this.invoked.add(DataFlow.getInstance().get(invoked));
    }

    public void overwriteInvoked(Set<DFGNode> invoked) {
        this.invoked = invoked;
    }

    @Override
    public Set<DFGNode> getCalls() {
        return calls;
    }

    @Override
    public void addToCalls(DFGNode callNode) {
        callNode.setInvoked(this.method);
        calls.add(callNode);
    }

    @Override
    public void addToCalls(MethodStatement call) {
        DFGNode node = new DFG_Node(call);
        node.setInvoked(this.method);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DFG_Node)) return false;

        DFG_Node dfgNode = (DFG_Node) o;

        return !(method != null ? !method.equals(dfgNode.method) : dfgNode.method != null);

    }

    @Override
    public int hashCode() {
        return method != null ? method.hashCode() : 0;
    }

    @Override
    public String toString() {
        String str = this.getMethodName() + "\n call -> {";
        for (DFGNode node : calls) {
            str = str + node.getMethodStatement().getName() + ", ";
        }
        str = str + "}\n invoked -> {";
        for (DFGNode node : invoked) {
            str = str + node.getMethodStatement().getName() + ", ";
        }
        str = str + "}\n";
        return str;
    }
}
