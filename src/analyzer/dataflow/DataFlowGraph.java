package analyzer.dataflow;

import analyzer.statements.Statement;

public interface DataFlowGraph {
    static DataFlowGraph getInstance() {
        return null;
    }

    DFGNode put(DFGNode node);
    DFGNode get(Statement statement);
    DFGNode get(int hashKey);
}
