package analyzer.ast.visitorsImpl;

import analyzer.ast.parser.Java8BaseVisitor;
import analyzer.ast.parser.Java8Parser;
import analyzer.statements.AbstractStatement;
import analyzer.statements.impl.statements.ClassDeclarationStatement;
import analyzer.statements.impl.statements.InterfaceDeclarationStatement;

public class MainVisitor<T> extends Java8BaseVisitor<T> {


    @Override
    public T visitNormalClassDeclaration(Java8Parser.NormalClassDeclarationContext ctx) {
        System.out.println("Class");
        ClassDeclarationStatement classDeclarationStatement = new ClassDeclarationStatement(ctx, this.statement);
        ClassDeclarationStatementVisitor visitor = new ClassDeclarationStatementVisitor(classDeclarationStatement);
        visitor.visit(ctx.classBody());

        statement.add(classDeclarationStatement);
        return null;
    }

    @Override
    public T visitNormalInterfaceDeclaration(Java8Parser.NormalInterfaceDeclarationContext ctx) {
        System.out.println("Interface");
        InterfaceDeclarationStatement interfaceDeclarationStatement = new InterfaceDeclarationStatement(ctx, this.statement);
        InterfaceDeclarationStatementVisitor visitor = new InterfaceDeclarationStatementVisitor(interfaceDeclarationStatement);
        visitor.visit(ctx.interfaceBody());

        statement.add(interfaceDeclarationStatement);
        return null;
    }

    public MainVisitor() {
        statement = new AbstractStatement() {
        };
    }
}
