package analyzer.ast.visitorsImpl;

import analyzer.ast.parser.Java8BaseVisitor;
import analyzer.ast.parser.Java8Parser;
import analyzer.dataflow.DFGNode;
import analyzer.dataflow.impl.dataflow.DFG_TNode;
import analyzer.dataflow.impl.dataflow.DataFlow;
import analyzer.statements.MethodStatement;
import analyzer.statements.Statement;
import analyzer.statements.impl.statements.MethodInvoked;

public class MethodDeclarationVisitor<T> extends Java8BaseVisitor<T> {

    public MethodDeclarationVisitor(Statement statement) {
        this.statement = statement;
    }

    @Override
    public T visitMethodInvocation(Java8Parser.MethodInvocationContext ctx) {
        System.out.println("\t" + ctx.getText());
        MethodInvoked methodInvoked = new MethodInvoked(ctx, statement);
        createDFGNode(methodInvoked);

        return null;
    }

    @Override
    public T visitMethodInvocation_lf_primary(Java8Parser.MethodInvocation_lf_primaryContext ctx) {
        System.out.println("\t" + ctx.getText());
        MethodInvoked methodInvoked = new MethodInvoked(ctx, statement);
        createDFGNode(methodInvoked);
        return null;
    }

    @Override
    public T visitMethodInvocation_lfno_primary(Java8Parser.MethodInvocation_lfno_primaryContext ctx) {
        System.out.println("\t" + ctx.getText());
        MethodInvoked methodInvoked = new MethodInvoked(ctx, statement);
        createDFGNode(methodInvoked);
        return null;
    }

    private void createDFGNode(Statement methodInvoked) {
        DFGNode node = new DFG_TNode(((MethodStatement) methodInvoked));
        node.setInvoked(statement);
        DataFlow.getInstance().get(statement).addToCalls(node);
//        System.out.println(node.hashCode());
        DataFlow.getInstance().put(node);
    }
}
