//package analyzer.ast.visitorsImpl;
//
//import analyzer.ast.parser.Java8BaseVisitor;
//import analyzer.ast.parser.Java8Parser;
//import AbstractStatement;
//import DataFlowVariable;
//import Finder;
//import AbstractVariable;
//
//import java.util.LinkedList;
//import java.util.List;
//
//public class ExpressionVisitor<T> extends Java8BaseVisitor<T>{
//
//    private boolean isCallMethod = false;
//
//    private List<AbstractVariable> queue = new LinkedList<>();
//
//    private List<AbstractVariable> variableStatementList;
//    private List<AbstractStatement> classDeclarationStatementList;
//    private List<AbstractStatement> methodDeclarationStatementList;
//    private List<AbstractStatement> interfaceDeclarationStatementList;
//
//    AbstractVariable variable;
//
//    @Override
//    public T visitMethodInvocation_lfno_primary(Java8Parser.MethodInvocation_lfno_primaryContext ctx) {
//        System.out.println("Find Method");
//        if (ctx.methodName() != null && ctx.argumentList() != null){
//            System.out.println("Simple call f()");
//            AbstractStatement statement = Finder.findStatement(ctx.methodName().Identifier().getText(), methodDeclarationStatementList);
//            if (statement != null) {
//                System.out.println("Find call: " + statement.getName());
//                for (Java8Parser.ExpressionContext expressionContext : ctx.argumentList().expression()) {
//                }
//                isCallMethod = true;
//            }
//
//        } else if (ctx.typeName() != null && ctx.argumentList() != null) {
//            System.out.println("Call a.f()");
//            if (ctx.typeName().packageOrTypeName() != null){
//
//            } else {
//                AbstractStatement statement = Finder.findStatement(ctx.typeName().Identifier().getText(), classDeclarationStatementList);
//                if (statement != null) {
//                    isCallMethod = true;
//                    System.out.println(statement.getName());
//                }
//            }
//        } else if (ctx.expressionName() != null && ctx.argumentList() != null) {
//            System.out.println("Call packet.a.f()");
//        } else if (ctx.argumentList() != null) {
//            System.out.println("Call super.f()");
//        } else {
//            System.out.println("Call a.super.f()");
//        }
//        return super.visitMethodInvocation_lfno_primary(ctx);
//    }
//
//    @Override
//    public T visitPostfixExpression(Java8Parser.PostfixExpressionContext ctx) {
//        if (ctx.expressionName() != null){
//            variable.getDataFlowVariable().getUsingOn().add(new DataFlowVariable(ctx.getStart()));
//            if (isCallMethod){
//                System.out.println("go into called method");
//            }
//            return null;
//        } else {
//            //ExpressionVisitor visitor = new ExpressionVisitor(variableStatementList, classDeclarationStatementList, methodDeclarationStatementList, variable);
//            //visitor.visit(ctx);
//        }
//        return super.visitPostfixExpression(ctx);
//    }
//
//    public ExpressionVisitor(
//            List<AbstractVariable> variableStatementList,
//            List<AbstractStatement> classDeclarationStatementList,
//            List<AbstractStatement> methodDeclarationStatementList,
//            AbstractVariable variable) {
//        this.variableStatementList = variableStatementList;
//        this.classDeclarationStatementList = classDeclarationStatementList;
//        this.methodDeclarationStatementList = methodDeclarationStatementList;
//        this.interfaceDeclarationStatementList = interfaceDeclarationStatementList;
//        this.variable = variable;
//    }
//
//}
