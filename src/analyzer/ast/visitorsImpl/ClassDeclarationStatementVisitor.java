package analyzer.ast.visitorsImpl;

import analyzer.ast.parser.Java8BaseVisitor;
import analyzer.ast.parser.Java8Parser;
import analyzer.dataflow.impl.dataflow.DFG_Node;
import analyzer.dataflow.impl.dataflow.DataFlow;
import analyzer.statements.Statement;
import analyzer.statements.impl.statements.ClassDeclarationStatement;
import analyzer.statements.impl.statements.InterfaceDeclarationStatement;
import analyzer.statements.impl.statements.MethodDeclarationStatement;

public class ClassDeclarationStatementVisitor<T> extends Java8BaseVisitor<T> {

    @Override
    public T visitMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) {
        System.out.println("Method " + ctx.methodHeader().getText());
        MethodDeclarationStatement methodDeclarationStatement = new MethodDeclarationStatement(ctx, this.statement);
        methodDeclarationStatement.setLine(ctx.getStart().getLine());
        MethodDeclarationStatementVisitor visitor = new MethodDeclarationStatementVisitor(methodDeclarationStatement);
        visitor.visit(ctx);

        statement.add(methodDeclarationStatement);
        DFG_Node node = new DFG_Node(methodDeclarationStatement);
        DataFlow.getInstance().put(node);

        MethodDeclarationVisitor declarationVisitor = new MethodDeclarationVisitor(methodDeclarationStatement);
        declarationVisitor.visitMethodBody(ctx.methodBody());

        return null;
    }

    @Override
    public T visitNormalClassDeclaration(Java8Parser.NormalClassDeclarationContext ctx) {
        System.out.println("Class");
        ClassDeclarationStatement classDeclarationStatement = new ClassDeclarationStatement(ctx, this.statement);
        classDeclarationStatement.setLine(ctx.getStart().getLine());
        ClassDeclarationStatementVisitor visitor = new ClassDeclarationStatementVisitor(classDeclarationStatement);
        visitor.visit(ctx.classBody());

        statement.add(classDeclarationStatement);
        return null;
    }

//    @Override
//    public T visitFieldDeclaration(Java8Parser.FieldDeclarationContext ctx) {
//        System.out.println("Field");
//        for (Java8Parser.VariableDeclaratorContext var : ctx.variableDeclaratorList().variableDeclarator()){
//            FieldClassVariable variable = new FieldClassVariable();
//
//            variable.setFieldModifier(ctx.fieldModifier());
//            variable.setUnannTypeContext(ctx.unannType());
//            variable.setDeclaratorContext(var.variableDeclaratorId());
//            variable.setBelongs(statement);
//            statement.getVariables().add(variable);
//        }
//        return super.visitFieldDeclaration(ctx);
//    }

    @Override
    public T visitNormalInterfaceDeclaration(Java8Parser.NormalInterfaceDeclarationContext ctx) {
        System.out.println("Interface");
        InterfaceDeclarationStatement interfaceDeclarationStatement = new InterfaceDeclarationStatement(ctx, this.statement);
        interfaceDeclarationStatement.setLine(ctx.getStart().getLine());
        InterfaceDeclarationStatementVisitor visitor = new InterfaceDeclarationStatementVisitor(interfaceDeclarationStatement);
        visitor.visit(ctx.interfaceBody());

        statement.add(interfaceDeclarationStatement);
        return null;
    }

    public ClassDeclarationStatementVisitor(Statement statement) {
        this.statement = statement;
    }

}