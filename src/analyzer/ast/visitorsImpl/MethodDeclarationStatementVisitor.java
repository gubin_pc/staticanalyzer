package analyzer.ast.visitorsImpl;

import analyzer.ast.parser.Java8BaseVisitor;
import analyzer.ast.parser.Java8Parser;
import analyzer.statements.AbstractStatement;
import analyzer.statements.impl.legacy.statement.BaseStatement;
import analyzer.statements.impl.statements.ClassDeclarationStatement;
import analyzer.statements.variables.ArgumentParameterVariable;

public class MethodDeclarationStatementVisitor<T> extends Java8BaseVisitor<T> {

    @Override
    public T visitFormalParameter(Java8Parser.FormalParameterContext ctx) {
        ArgumentParameterVariable variable = new ArgumentParameterVariable();
        variable.setVariableModifier(ctx.variableModifier());
        variable.setUnannTypeContext(ctx.unannType());
        variable.setDeclaratorContext(ctx.variableDeclaratorId());
        variable.setBelongs(this.statement);
        this.statement.getVariables().add(variable);
        return super.visitFormalParameter(ctx);
    }

    @Override
    public T visitLastFormalParameter(Java8Parser.LastFormalParameterContext ctx) {
        if (ctx.formalParameter() == null) {
            ArgumentParameterVariable variable = new ArgumentParameterVariable();
            variable.setVariableModifier(ctx.variableModifier());
            variable.setUnannTypeContext(ctx.unannType());
            variable.setDeclaratorContext(ctx.variableDeclaratorId());
            variable.setBelongs(this.statement);
            this.statement.getVariables().add(variable);
        }
        return super.visitLastFormalParameter(ctx);
    }

//    @Override
//    public T visitLocalVariableDeclaration(Java8Parser.LocalVariableDeclarationContext ctx) {
//        for (Java8Parser.VariableDeclaratorContext var : ctx.variableDeclaratorList().variableDeclarator()) {
//            //System.out.println("LocalVar = " + var.getText());
//            LocalVariable variable = new LocalVariable();
//            variable.setVariableModifier(ctx.variableModifier());
//            variable.setUnannTypeContext(ctx.unannType());
//            variable.setDeclaratorContext(var.variableDeclaratorId());
//            variable.setBelongs(statement);
//
//            statement.getVariables().add(variable);
//        }
//        return super.visitLocalVariableDeclaration(ctx);
//    }

    @Override
    public T visitNormalClassDeclaration(Java8Parser.NormalClassDeclarationContext ctx) {
        System.out.println("Class");
        ClassDeclarationStatement classDeclarationStatement = new ClassDeclarationStatement(ctx, this.statement);
        classDeclarationStatement.setLine(ctx.getStart().getLine());
        ClassDeclarationStatementVisitor visitor = new ClassDeclarationStatementVisitor(classDeclarationStatement);
        visitor.visit(ctx.classBody());

        statement.add(classDeclarationStatement);
        return null;
    }

    public MethodDeclarationStatementVisitor() {
        statement = new BaseStatement();
    }

    public MethodDeclarationStatementVisitor(AbstractStatement statement) {
        this.statement = statement;
    }
}
