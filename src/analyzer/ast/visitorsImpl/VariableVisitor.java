//package analyzer.ast.visitorsImpl;
//
//import analyzer.ast.parser.Java8BaseVisitor;
//import analyzer.ast.parser.Java8Parser;
//import AbstractStatement;
//import DataFlowVariable;
//import Finder;
//import AbstractVariable;
//import org.antlr.v4.runtime.tree.ParseTree;
//
//import java.util.LinkedList;
//import java.util.List;
//
//public class VariableVisitor<T> extends Java8BaseVisitor<T> {
//
//    private List<AbstractVariable> queue = new LinkedList<>();
//
//    private List<AbstractVariable> variableStatementList;
//    private List<AbstractStatement> classDeclarationStatementList;
//    private List<AbstractStatement> methodDeclarationStatementList;
//    private List<AbstractStatement> interfaceDeclarationStatementList;
//    private AbstractVariable variable;
//
//    @Override
//    public T visitVariableDeclarator(Java8Parser.VariableDeclaratorContext ctx) {
//        if (variable.getDeclaratorIdContext().getStart().equals(ctx.variableDeclaratorId().getStart())) {
//            DataFlowVariable flowVariable = new DataFlowVariable(ctx.variableDeclaratorId().getStart());
//            variable.setDataFlowVariable(flowVariable);
//        }
//        return super.visitVariableDeclarator(ctx);
//    }
//
//    @Override
//    public T visitExpressionName(Java8Parser.ExpressionNameContext ctx) {
//        if (ctx.getText().equals(variable.getName())) {
//            System.out.println("\nVariableVisitorTest: expressionName " + ctx.getStart());
//            analyzeFullStatement(ctx);
//        }
//        return super.visitExpressionName(ctx);
//    }
//
//    private void analyzeFullStatement(ParseTree ctx) {
//        if (ctx instanceof Java8Parser.VariableDeclaratorContext) {
//            //TODO issue https://bitbucket.org/gubin_pc/staticanalyzer/issue/2
//            String variableName = ((Java8Parser.VariableDeclaratorContext) ctx).variableDeclaratorId().Identifier().getText();
//            System.out.println("VariableVisitorTest: initVar " + variableName);
//            AbstractVariable tmp = Finder.findVariableRecurrentDown(variable.getBelongs(), variableStatementList, variableName);
//            if (tmp != null) {
//                System.out.println("I FIND IT:" + tmp.getName());
//                queue.add(tmp);
//            }
//            ExpressionVisitor visitor = new ExpressionVisitor(variableStatementList, classDeclarationStatementList, methodDeclarationStatementList, variable);
//            visitor.visit(ctx);
//        } else if (ctx instanceof Java8Parser.AssignmentContext) {
//            String leftHandSide = ((Java8Parser.AssignmentContext) ctx).leftHandSide().expressionName().getText();
//            if (!leftHandSide.equals(variable.getName())) {
//                System.out.println("VariableVisitorTest: " + leftHandSide);
//                AbstractVariable tmp = Finder.findVariableRecurrentUp(variable.getBelongs(), variableStatementList, leftHandSide);
//                if (tmp != null) {
//                    System.out.println("I FIND IT:" + tmp.getName());
//                    queue.add(tmp);
//                }
//                ExpressionVisitor visitor = new ExpressionVisitor(variableStatementList, classDeclarationStatementList, methodDeclarationStatementList, variable);
//                visitor.visit(ctx);
//            }
//            //TODO issue https://bitbucket.org/gubin_pc/staticanalyzer/issue/1
//        } else {
//            analyzeFullStatement(ctx.getParent());
//        }
//    }
//
//    private void analyzeExpression(ParseTree ctx){
//
//    }
//
//
//    public VariableVisitor(
//            List<AbstractVariable> variableStatementList,
//            List<AbstractStatement> classDeclarationStatementList,
//            List<AbstractStatement> methodDeclarationStatementList,
//            AbstractVariable variable) {
//        this.variableStatementList = variableStatementList;
//        this.classDeclarationStatementList = classDeclarationStatementList;
//        this.methodDeclarationStatementList = methodDeclarationStatementList;
//        this.interfaceDeclarationStatementList = interfaceDeclarationStatementList;
//        this.variable = variable;
//    }
//}
