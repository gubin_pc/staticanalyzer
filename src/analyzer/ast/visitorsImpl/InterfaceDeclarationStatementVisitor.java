package analyzer.ast.visitorsImpl;

import analyzer.ast.parser.Java8BaseVisitor;
import analyzer.ast.parser.Java8Parser;
import analyzer.dataflow.impl.dataflow.DFG_Node;
import analyzer.dataflow.impl.dataflow.DataFlow;
import analyzer.statements.AbstractStatement;
import analyzer.statements.impl.legacy.statement.BaseStatement;
import analyzer.statements.impl.statements.ClassDeclarationStatement;
import analyzer.statements.impl.statements.InterfaceDeclarationStatement;
import analyzer.statements.impl.statements.MethodDeclarationStatement;

public class InterfaceDeclarationStatementVisitor<T> extends Java8BaseVisitor<T>{

    @Override
    public T visitNormalClassDeclaration(Java8Parser.NormalClassDeclarationContext ctx) {
        System.out.println("Class");
        ClassDeclarationStatement classDeclarationStatement = new ClassDeclarationStatement(ctx, this.statement);
        classDeclarationStatement.setLine(ctx.getStart().getLine());
        ClassDeclarationStatementVisitor visitor = new ClassDeclarationStatementVisitor(classDeclarationStatement);
        visitor.visit(ctx.classBody());

        statement.add(classDeclarationStatement);

        return null;
    }

    @Override
    public T visitNormalInterfaceDeclaration(Java8Parser.NormalInterfaceDeclarationContext ctx) {
        System.out.println("Interface");
        InterfaceDeclarationStatement interfaceDeclarationStatement = new InterfaceDeclarationStatement(ctx, this.statement);
        interfaceDeclarationStatement.setLine(ctx.getStart().getLine());
        InterfaceDeclarationStatementVisitor visitor = new InterfaceDeclarationStatementVisitor(interfaceDeclarationStatement);
        visitor.visit(ctx.interfaceBody());

        statement.add(interfaceDeclarationStatement);

        return null;
    }

    @Override
    public T visitInterfaceMethodDeclaration(Java8Parser.InterfaceMethodDeclarationContext ctx) {
        System.out.println("Method " + ctx.methodHeader().getText());
        MethodDeclarationStatement methodDeclarationStatement = new MethodDeclarationStatement(ctx, this.statement);
        methodDeclarationStatement.setLine(ctx.getStart().getLine());
        MethodDeclarationStatementVisitor visitor = new MethodDeclarationStatementVisitor(methodDeclarationStatement);
        visitor.visit(ctx);

        statement.add(methodDeclarationStatement);
        DFG_Node node = new DFG_Node(methodDeclarationStatement);
        DataFlow.getInstance().put(node);

        MethodDeclarationVisitor declarationVisitor = new MethodDeclarationVisitor(methodDeclarationStatement);
        declarationVisitor.visitMethodBody(ctx.methodBody());

        return null;
    }

//    @Override
//    public T visitConstantDeclaration(Java8Parser.ConstantDeclarationContext ctx) {
//        System.out.println("Field");
//        for (Java8Parser.VariableDeclaratorContext var : ctx.variableDeclaratorList().variableDeclarator()) {
//            ConstantVariable variable = new ConstantVariable();
//
//            variable.setConstantModifier(ctx.constantModifier());
//            variable.setUnannTypeContext(ctx.unannType());
//            variable.setDeclaratorContext(var.variableDeclaratorId());
//            variable.setBelongs(statement);
//            statement.getVariables().add(variable);
//        }
//        return super.visitConstantDeclaration(ctx);
//    }

    public InterfaceDeclarationStatementVisitor() {
        statement = new BaseStatement();
    }

    public InterfaceDeclarationStatementVisitor(AbstractStatement statement) {
        this.statement = statement;
    }
}
