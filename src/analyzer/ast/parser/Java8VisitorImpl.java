package analyzer.ast.parser;

/**
 * Created by Gubin on 07/06/15.
 */
public class Java8VisitorImpl<T> extends Java8BaseVisitor<T> {
    @Override
    public T visitIntegralType(Java8Parser.IntegralTypeContext ctx) {
        return super.visitIntegralType(ctx);
    }

    @Override
    public T visitUnannType(Java8Parser.UnannTypeContext ctx) {
        return super.visitUnannType(ctx);
    }

    @Override
    public T visitBlockStatement(Java8Parser.BlockStatementContext ctx) {
       // System.out.println(ctx.getStart());
        return super.visitBlockStatement(ctx);
    }

    @Override
    public T visitBlock(Java8Parser.BlockContext ctx) {
        return super.visitBlock(ctx);
    }
}
